package com.robbederks.railfever.Rails.RailPiece;

import com.robbederks.railfever.Rails.RailState;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class RailPiece extends Entity implements ITouchArea{
    //Constants
    public static final int RESOLUTION = 4;
    protected static final int DEBUGFACTOR = 2;
    protected static final float TRANSPARENCY = 0.4f;

    //Abstract methods
    public abstract int getLength();
    public abstract RailState getState(int index, int fromConnection);
    public abstract Entity getDebugEntity(VertexBufferObjectManager manager);
    public abstract int getNextPieceIndex(int fromConnection);
    public abstract int getNextPieceConnection(int fromConnection);

    //Attributes
    private final VertexBufferObjectManager vertexBufferObjectManager;
    private final Texture texture;
    private final TextureRegion track;

    //Constructors
    public RailPiece(VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        this.vertexBufferObjectManager = vertexBufferObjectManager;
        this.texture = texture;
        this.track = track;
    }

    //Methods
    protected void generateEntity(){
        generateEntity(1.0f);
    }

    protected void generateEntity(float alpha){
        SpriteBatch spriteBatch = new SpriteBatch(texture, (int)(getLength() * RESOLUTION / track.getWidth()), vertexBufferObjectManager);

        int xOffset = Math.round(track.getWidth()/2);
        int yOffset = Math.round(track.getHeight()/2);

        for(int i=0; i<(int)(getLength() * RESOLUTION / track.getWidth()); i++){
            RailState state = getState(Math.round(i * track.getWidth() / (float) RESOLUTION), 0);
            spriteBatch.draw(track, state.x - xOffset, state.y - yOffset, track.getWidth(), track.getHeight(), (float) (state.rot * 180 / RailState.PI), 1, 1, 1, alpha);
        }

        spriteBatch.submit();
        attachChild(spriteBatch);
    }

    @Override
    public boolean contains(float pX, float pY) {
        return false;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        return false;
    }
}
