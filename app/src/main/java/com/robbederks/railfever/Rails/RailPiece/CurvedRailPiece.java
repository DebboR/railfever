package com.robbederks.railfever.Rails.RailPiece;

import com.robbederks.railfever.Rails.RailState;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

public class CurvedRailPiece extends RailPiece{

    private ArrayList<RailState> states;
    private int prevPiece, nextPiece, prevConnection, nextConnection;

    public CurvedRailPiece(RailState start, RailState end, int prevPiece, int prevConnection, int nextPiece, int nextConnection, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        super(vertexBufferObjectManager, texture, track);

        generateStates(start, end);
        this.prevPiece = prevPiece;
        this.prevConnection = prevConnection;
        this.nextPiece = nextPiece;
        this.nextConnection = nextConnection;

        generateEntity();
    }

    private void generateStates(RailState start, RailState end){
        //Generate bisector
        RailState bisector = start.intersection(end);

        RailState n1 = start.getNormal();
        RailState n2 = end.getNormal();

        //Generate both intersection points with bisector
        RailState s1 = n1.intersection(bisector);
        RailState s2 = n2.intersection(bisector);

        //Check which has smallest radius to draw (and if same, get nearest point to bisector)
        if((s1.getDistance(start) < s2.getDistance(end)) || (s1.getDistance(start) == s2.getDistance(end) && start.getDistance(bisector) < end.getDistance(bisector))){
            //Generate radius starting from start
            if(start.sideOfLine(end)) {
                states = generateCurve(new RailState(s1.x, s1.y, n1.rot + RailState.PI), Math.round(s1.getDistance(start)), RailState.normalizedAngle(n2.rot), true);
            } else {
                states = generateCurve(new RailState(s1.x, s1.y, n1.rot), Math.round(s1.getDistance(start)), RailState.normalizedAngle(n2.rot + RailState.PI), false);
            }

            //Add straight line
            if(states.size() > 0)
                states.addAll(generateStraight(states.get(states.size()-1).x, states.get(states.size()-1).y, end.x, end.y));
            else
                states.addAll(generateStraight(start.x, start.y, end.x, end.y));
        } else {
            //Generate radius starting from end
            if(start.sideOfLine(end)) {
                states = generateCurve(new RailState(s2.x, s2.y, n1.rot + RailState.PI), Math.round(s2.getDistance(end)), RailState.normalizedAngle(n2.rot), true);
            } else {
                states = generateCurve(new RailState(s2.x, s2.y, n1.rot), Math.round(s2.getDistance(end)), RailState.normalizedAngle(n2.rot + RailState.PI), false);
            }

            //Add straight line
            if(states.size() > 0)
                states.addAll(0, generateStraight(start.x, start.y, states.get(0).x, states.get(0).y));
            else
                states.addAll(generateStraight(start.x, start.y, end.x, end.y));
        }

    }

    private ArrayList<RailState> generateCurve(RailState center, int radius, double stopAngle, boolean clockwise){
        ArrayList<RailState> res = new ArrayList<>();

        if(radius == 0)
            return res;

        double startResAngle = RailState.normalizedAngle(center.rot - RailState.HALFPI);
        double stopResAngle = RailState.normalizedAngle(stopAngle - RailState.HALFPI);
        if(!clockwise){
            double diff = RailState.normalizedAngle(stopResAngle + RailState.TWOPI - startResAngle);
            for(double a = 0; a < diff; a += (double)RESOLUTION/radius){
                res.add(new RailState(center.x + (int)Math.round(radius * Math.cos(center.rot + a)), center.y + (int)Math.round(radius * Math.sin(center.rot + a)), startResAngle + a));
            }
        } else {
            double diff = RailState.normalizedAngle(startResAngle + RailState.TWOPI - stopResAngle);
            for(double a = 0; a < diff; a += (double)RESOLUTION/radius){
                res.add(new RailState(center.x + (int)Math.round(radius * Math.cos(center.rot - a)), center.y + (int)Math.round(radius * Math.sin(center.rot - a)), startResAngle - a));
            }
        }
        return res;
    }

    private ArrayList<RailState> generateStraight(int startX, int startY, int endX, int endY){
        StraightRailPiece temp = new StraightRailPiece(startX, startY, endX, endY);
        ArrayList<RailState> res = new ArrayList<>(temp.getLength());
        for(int i=0; i<temp.getLength(); i++){
            res.add(temp.getState(i, 0));
        }
        return res;
    }

    @Override
    public int getLength(){
        return states.size();
    }

    @Override
    public RailState getState(int index, int fromConnection){
        switch (fromConnection){
            case 0:
                return states.get(index);
            case 1:
                RailState temp = states.get(Math.max(states.size() - index - 1, 0));
                temp.rot = RailState.normalizedAngle(temp.rot + RailState.PI);
                return temp;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public Entity getDebugEntity(VertexBufferObjectManager manager) {
        Entity res = new Entity();
        int i;
        for(i=DEBUGFACTOR; i<states.size(); i+=DEBUGFACTOR){
            Line line = new Line(states.get(i - DEBUGFACTOR).x, states.get(i - DEBUGFACTOR).y, states.get(i).x, states.get(i).y, manager, DrawType.STATIC);
            line.setColor(Color.BLUE);
            res.attachChild(line);
        }
        Line line = new Line(states.get(i - DEBUGFACTOR).x, states.get(i - DEBUGFACTOR).y, states.get(states.size() - 1).x, states.get(states.size() - 1).y, manager, DrawType.STATIC);
        line.setColor(Color.BLUE);
        res.attachChild(line);
        return res;
    }

    @Override
    public int getNextPieceIndex(int fromConnection) {
        switch (fromConnection){
            case 0:
                return nextPiece;
            case 1:
                return prevPiece;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public int getNextPieceConnection(int fromConnection) {
        switch (fromConnection){
            case 0:
                return nextConnection;
            case 1:
                return prevConnection;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }
}
