package com.robbederks.railfever.Rails.RailPiece;

import com.robbederks.railfever.Rails.RailState;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class SwitchRailPiece extends RailPiece{

    private int prevPiece, prevConnection, nextPiece1, nextConnection1, nextPiece2, nextConnection2;
    private boolean isSwitched = false;

    private CurvedRailPiece path1, path2;
    private SpriteBatch sbOpaque1, sbOpaque2, sbTransparent1, sbTransparent2;
    private int minX, maxX, minY, maxY;

    public SwitchRailPiece(RailState start, RailState end1, RailState end2, int prevPiece, int prevConnection, int nextPiece1, int nextConnection1, int nextPiece2, int nextConnection2, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track) {
        super(vertexBufferObjectManager, texture, track);
        path1 = new CurvedRailPiece(start, end1, prevPiece, prevConnection, nextPiece1, nextConnection1, vertexBufferObjectManager, texture, track);
        path2 = new CurvedRailPiece(start, end2, prevPiece, prevConnection, nextPiece2, nextConnection2, vertexBufferObjectManager, texture, track);

        this.prevPiece = prevPiece;
        this.prevConnection = prevConnection;
        this.nextPiece1 = nextPiece1;
        this.nextConnection1 = nextConnection1;
        this.nextPiece2 = nextPiece2;
        this.nextConnection2 = nextConnection2;

        calculateBorders();
        generateEntity();
    }

    public void switchRails(){
        isSwitched = !isSwitched;
        detachChildren();

        sbOpaque1.detachSelf();
        sbOpaque2.detachSelf();
        sbTransparent1.detachSelf();
        sbTransparent2.detachSelf();

        if(isSwitched) {
            attachChild(sbTransparent1);
            attachChild(sbOpaque2);
        } else {
            attachChild(sbOpaque1);
            attachChild(sbTransparent2);
        }
    }

    private void calculateBorders(){
        RailState t = path1.getState(0, 0);
        minX = t.x;
        maxX = t.x;
        minY = t.y;
        maxY = t.y;
        for(int i=0; i<path1.getLength(); i++){
            t = path1.getState(i, 0);
            minX = Math.min(t.x, minX);
            minY = Math.min(t.y, minY);
            maxX = Math.max(t.x, maxX);
            maxY = Math.max(t.y, maxY);
        }
        for(int i=0; i<path2.getLength(); i++){
            t = path2.getState(i, 0);
            minX = Math.min(t.x, minX);
            minY = Math.min(t.y, minY);
            maxX = Math.max(t.x, maxX);
            maxY = Math.max(t.y, maxY);
        }
    }

    @Override
    public int getLength() {
        if(isSwitched)
            return path2.getLength();
        return path1.getLength();
    }

    @Override
    public RailState getState(int index, int fromConnection) {
        switch(fromConnection){
            case 0:
                if(isSwitched)
                    return path2.getState(index, 0);
                return path1.getState(index, 0);
            case 1:
                return path1.getState(index, 1);
            case 2:
                return path2.getState(index, 1);
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public Entity getDebugEntity(VertexBufferObjectManager manager) {
        Entity res = new Entity();
        res.attachChild(path1.getDebugEntity(manager));
        res.attachChild(path2.getDebugEntity(manager));
        res.setColor(Color.RED);
        return res;
    }

    @Override
    public int getNextPieceIndex(int fromConnection) {
        switch(fromConnection){
            case 0:
                if(isSwitched)
                    return nextPiece2;
                return nextPiece1;
            case 1:
                return prevPiece;
            case 2:
                return prevPiece;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public int getNextPieceConnection(int fromConnection) {
        switch(fromConnection){
            case 0:
                if(isSwitched)
                    return nextConnection2;
                return nextConnection1;
            case 1:
                return prevConnection;
            case 2:
                return prevConnection;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public void generateEntity(){

        super.generateEntity();
        sbOpaque1 = (SpriteBatch) getChildByIndex(0);
        detachChildren();

        isSwitched = !isSwitched;
        super.generateEntity();
        sbOpaque2 = (SpriteBatch) getChildByIndex(0);
        detachChildren();
        isSwitched = !isSwitched;

        super.generateEntity(TRANSPARENCY);
        sbTransparent1 = (SpriteBatch) getChildByIndex(0);
        detachChildren();

        isSwitched = !isSwitched;
        super.generateEntity(TRANSPARENCY);
        sbTransparent2 = (SpriteBatch) getChildByIndex(0);
        detachChildren();
        isSwitched = !isSwitched;

        attachChild(sbOpaque1);
        attachChild(sbTransparent2);
    }

    @Override
    public boolean contains(float pX, float pY) {
        return (pX >= minX && pX <= maxX && pY >= minY && pY <= maxY);
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP)
            switchRails();
        return true;
    }
}
