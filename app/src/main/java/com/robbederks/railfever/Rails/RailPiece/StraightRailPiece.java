package com.robbederks.railfever.Rails.RailPiece;

import com.robbederks.railfever.Rails.RailState;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

public class StraightRailPiece extends RailPiece{
    private RailState start;
    private RailState end;
    private int distance;

    private int prevPiece, nextPiece, prevConnection, nextConnection;

    public StraightRailPiece(int startX, int startY, int endX, int endY){
        this(startX, startY, endX, endY, 0, 0, 0, 0, null, null, null);
    }

    public StraightRailPiece(int startX, int startY, int endX, int endY, int prevPiece, int prevConnection, int nextPiece, int nextConnection, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        super(vertexBufferObjectManager, texture, track);
        double angle;
        if(endX == startX){
            angle = startY < endY ? RailState.HALFPI : 3*RailState.HALFPI;
        } else {
            angle = Math.atan((double)((endY - startY))/(endX - startX));
            if(startX > endX){
                angle = RailState.normalizedAngle(angle + RailState.PI);
            }
        }
        this.start = new RailState(startX, startY, angle);
        this.end = new RailState(endX, endY, RailState.PI + angle);
        this.distance = start.getDistance(end) / RESOLUTION;

        this.prevPiece = prevPiece;
        this.prevConnection = prevConnection;
        this.nextPiece = nextPiece;
        this.nextConnection = nextConnection;

        if(vertexBufferObjectManager != null)
            generateEntity();
    }

    @Override
    public int getLength(){
        return distance;
    }

    @Override
    public RailState getState(int index, int fromConnection){
        switch(fromConnection){
            case 0:
                return new RailState(start.x + (int)Math.round(Math.cos(start.rot) * index * RESOLUTION), start.y + (int)Math.round(Math.sin(start.rot) * index * RESOLUTION), start.rot);
            case 1:
                return new RailState(end.x + (int)Math.round(Math.cos(end.rot) * index * RESOLUTION), end.y + (int)Math.round(Math.sin(end.rot) * index * RESOLUTION), end.rot);
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public Entity getDebugEntity(VertexBufferObjectManager manager) {
        Line res = new Line(start.x, start.y, end.x, end.y, manager);
        res.setColor(Color.BLACK);
        return res;
    }

    @Override
    public int getNextPieceIndex(int fromConnection) {
        switch (fromConnection){
            case 0:
                return nextPiece;
            case 1:
                return prevPiece;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }

    @Override
    public int getNextPieceConnection(int fromConnection) {
        switch (fromConnection){
            case 0:
                return nextConnection;
            case 1:
                return prevConnection;
            default:
                throw new IllegalArgumentException("Connection #" + fromConnection + " doesn't exist in a " + this.getClass().getSimpleName());
        }
    }
}
