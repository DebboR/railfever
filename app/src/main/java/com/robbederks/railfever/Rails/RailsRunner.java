package com.robbederks.railfever.Rails;

import java.util.Timer;
import java.util.TimerTask;

public class RailsRunner {

    private final Rails rails;
    private int currentPiece, currentConnection;
    private final int length, startPiece, startConnection, startIndex;
    private double currentIndex, speed;
    private Timer speedTimer;
    private Boolean speedTimerRunning;

    public RailState current;
    public double baseSpeed;

    public int frontRailPiece, frontRailConnection, frontRailIndex;

    public RailsRunner(Rails rails, int startPiece, int startConnection, int startIndex, int length, float speed){
        this.rails = rails;
        this.startPiece = startPiece;
        this.startConnection = startConnection;
        this.startIndex = startIndex;
        this.length = length;
        this.speed = speed;

        this.baseSpeed = speed;
        this.speedTimer = new Timer();
        this.speedTimerRunning = false;

        resetRailsRunner();
        next();
    }

    public RailState next(){
        currentIndex += speed;
        RailState frontState, backState;
        while(((int) Math.round(currentIndex)) > rails.pieces.get(currentPiece).getLength() - 1){
            currentIndex -= rails.pieces.get(currentPiece).getLength();
            int tempPiece = currentPiece;
            currentPiece = rails.pieces.get(tempPiece).getNextPieceIndex(currentConnection);
            currentConnection = rails.pieces.get(tempPiece).getNextPieceConnection(currentConnection);
        }
        backState = rails.pieces.get(currentPiece).getState((int) Math.round(currentIndex), currentConnection);

        double nextIndex = currentIndex + length;
        frontRailIndex = (int) Math.round(nextIndex);
        frontRailPiece = currentPiece;
        frontRailConnection = currentConnection;
        while(((int) Math.round(nextIndex)) > rails.pieces.get(frontRailPiece).getLength() - 1){
            nextIndex -= rails.pieces.get(frontRailPiece).getLength();
            int tempPiece = frontRailPiece;
            frontRailPiece = rails.pieces.get(tempPiece).getNextPieceIndex(frontRailConnection);
            frontRailConnection = rails.pieces.get(tempPiece).getNextPieceConnection(frontRailConnection);
        }
        frontState = rails.pieces.get(frontRailPiece).getState((int) Math.round(nextIndex), frontRailConnection);

        if(frontState.x > backState.x)
            current = new RailState(Math.round((frontState.x + backState.x)/ 2.0f), Math.round((frontState.y + backState.y)/ 2.0f), Math.atan(((float)(frontState.y - backState.y))/(frontState.x - backState.x)));
        else if(Math.abs(frontState.x - backState.x) < 0.001)
            current = new RailState(frontState.x, Math.round((frontState.y + backState.y)/ 2.0f), frontState.y > backState.y ? RailState.HALFPI : RailState.HALFPI + RailState.PI);
        else
            current = new RailState(Math.round((frontState.x + backState.x)/ 2.0f), Math.round((frontState.y + backState.y)/ 2.0f), RailState.PI + Math.atan(((float)(frontState.y - backState.y))/(frontState.x - backState.x)));
        return current;
    }

    public void resetRailsRunner(){
        this.currentPiece = startPiece;
        this.currentConnection = startConnection;
        this.currentIndex = startIndex;
    }

    public void turn(){
        if (currentConnection == 0)
            currentConnection = 1;
        else
            currentConnection = 0;
        currentIndex = rails.pieces.get(currentPiece).getLength() - currentIndex;
    }

    public void setSpeed(double newSpeed, int duration){
        if(!speedTimerRunning){
            this.speed = newSpeed;
            speedTimerRunning = true;
            speedTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    speed = baseSpeed;
                    speedTimerRunning = false;
                }
            }, duration);
        }
    }
}
