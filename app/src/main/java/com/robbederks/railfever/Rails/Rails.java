package com.robbederks.railfever.Rails;

import com.robbederks.railfever.Rails.RailPiece.RailPiece;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.List;

public class Rails extends Entity implements ITouchArea{

    public List<RailPiece> pieces;

    public Rails(String fileName, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        pieces = RailsParser.loadXML(fileName, vertexBufferObjectManager, texture, track);

        for (RailPiece r : pieces)
            this.attachChild(r);
    }

    public Entity getDebugEntity(VertexBufferObjectManager manager){
        Entity res = new Entity();
        for(RailPiece r : pieces)
            res.attachChild(r.getDebugEntity(manager));
        return res;
    }

    @Override
    public boolean contains(float pX, float pY) {
        return true;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        for(int i=0; i<getChildCount(); i++){
            RailPiece t = ((RailPiece) getChildByIndex(i));
            if(t.contains(pTouchAreaLocalX, pTouchAreaLocalY))
                t.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
        }
        return false;
    }
}
