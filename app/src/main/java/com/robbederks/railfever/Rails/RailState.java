package com.robbederks.railfever.Rails;

public class RailState{

    public final static double HALFPI = Math.PI / 2;
    public final static double PI = Math.PI;
    public final static double TWOPI = 2*Math.PI;

    public int x, y;
    public double rot;

    public RailState(){}

    public RailState(int x, int y, double rot){
        this.x = x;
        this.y = y;
        this.rot = normalizedAngle(rot);
    }

    public int getDistance(RailState other){
        return (int) Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
    }

    public boolean isVertical(){
        return Math.abs(normalizedAngle(rot - Math.PI/2)%Math.PI) < 0.001;
    }

    public RailState intersection(RailState other){
        RailState res = new RailState();
        res.rot = (this.rot + other.rot)/2;
        if(Math.abs(this.rot%PI) < 0.001 && Math.abs(other.rot%PI) < 0.001){
            if(Math.abs(this.rot - PI) < 0.001)
                res.x = Math.max(this.x, other.x);
            else
                res.x = Math.min(this.x, other.x);
            res.y = (int) Math.round((this.y + other.y) / 2.0);
        } else if(this.isVertical() && other.isVertical()){
            res.x = (int) Math.round((this.x + other.x) / 2.0);
            if(Math.abs(this.rot - HALFPI) < 0.001)
                res.y = Math.max(this.y, other.y);
            else
                res.y = Math.min(this.y, other.y);
        } else if(this.isVertical()){
            res.x = this.x;
            res.y = other.y + (int)Math.round((this.x - other.x)*Math.tan(other.rot));
        } else if(other.isVertical()){
            res.x = other.x;
            res.y = this.y + (int)Math.round((other.x - this.x)*Math.tan(this.rot));
        } else {
            double m1 = Math.tan(this.rot);
            double m2 = Math.tan(other.rot);
            res.x = (int)Math.round((((m1*this.x) - (m2*other.x) - this.y + other.y)/(m1 - m2)));
            res.y = (int)Math.round((m2*this.y - m1*other.y + (m1*m2)*(other.x - this.x))/(m2 - m1));
        }
        return res;
    }

    public RailState getNormal(){
        return new RailState(this.x, this.y, this.rot + (Math.PI/2));
    }

    public RailState getMoved(int dist){
        return new RailState(this.x + (int)Math.round(dist*Math.cos(this.rot)), this.y + (int)Math.round(dist*Math.sin(this.rot)), this.rot);
    }

    public boolean sideOfLine(RailState other){
        RailState second = getMoved(20);
        return (((second.x - this.x)*(other.y - this.y)) - ((second.y - this.y)*(other.x - this.x))) > 0;
    }

    public static double normalizedAngle(double a){
        while(a < 0)
            a += TWOPI;
        return a % TWOPI;
    }

    public boolean equals(RailState other){
        return this.x == other.x && this.y == other.y;
    }
}
