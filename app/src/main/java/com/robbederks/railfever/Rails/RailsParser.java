package com.robbederks.railfever.Rails;

import android.util.Log;

import com.robbederks.railfever.LevelHelper;
import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.RailPiece.CurvedRailPiece;
import com.robbederks.railfever.Rails.RailPiece.RailPiece;
import com.robbederks.railfever.Rails.RailPiece.StraightRailPiece;
import com.robbederks.railfever.Rails.RailPiece.SwitchRailPiece;

import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.util.ArrayList;
import java.util.List;

public class RailsParser {

    //Globals
    private static List<RailPiece> result;
    private static VertexBufferObjectManager vertexBufferObjectManager;
    private static Texture texture;
    private static TextureRegion track;

    public static List<RailPiece> loadXML(String fileName, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){

        RailsParser.vertexBufferObjectManager = vertexBufferObjectManager;
        RailsParser.texture = texture;
        RailsParser.track = track;

        XmlPullParserFactory pullParserFactory;
        XmlPullParser pullParser;
        result = new ArrayList<>();

        try{
            pullParserFactory = XmlPullParserFactory.newInstance();
            pullParser = pullParserFactory.newPullParser();
            pullParser.setInput(RailFeverActivity.context.getAssets().open(LevelHelper.FOLDER + "/" + fileName), null);

            while(!(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.RailsName))){
                pullParser.next();
            }

            while(pullParser.getEventType() != XmlPullParser.END_DOCUMENT && !(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.RailsName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG){
                    switch(pullParser.getName()){
                        case LevelHelper.StraightRPName:
                            loadStraightRailPiece(pullParser);
                            break;
                        case LevelHelper.CurvedRPName:
                            loadCurvedRailPiece(pullParser);
                            break;
                        case LevelHelper.SwitchRPName:
                            loadSwitchRailPiece(pullParser);
                            break;
                        default:
                            break;
                    }
                }
                pullParser.next();
            }
        } catch(Exception e) {
            Log.e("RailsParser", "Error in loading level from file: " + fileName);
        }

        return result;
    }

    private static void loadStraightRailPiece(XmlPullParser pullParser){
        int startX=0, startY=0, endX=0, endY=0, prevPiece=0, prevConnection=0, nextPiece=0, nextConnection=0;
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.StraightRPName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.StartPointName)){
                    pullParser.next();
                    String[] coords = pullParser.getText().split(",");
                    startX = Integer.parseInt(coords[0]);
                    startY = Integer.parseInt(coords[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.EndPointName)){
                    pullParser.next();
                    String[] coords = pullParser.getText().split(",");
                    endX = Integer.parseInt(coords[0]);
                    endY = Integer.parseInt(coords[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.PreviousName)){
                    pullParser.next();
                    String[] prev = pullParser.getText().split(",");
                    prevPiece = Integer.parseInt(prev[0]);
                    prevConnection = Integer.parseInt(prev[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.NextName)){
                    pullParser.next();
                    String[] next = pullParser.getText().split(",");
                    nextPiece = Integer.parseInt(next[0]);
                    nextConnection = Integer.parseInt(next[1]);
                }
                pullParser.next();
            }
            result.add(new StraightRailPiece(startX, startY,
                    endX, endY,
                    prevPiece, prevConnection,
                    nextPiece, nextConnection,
                    vertexBufferObjectManager, texture, track));
        } catch (Exception e) {
            Log.e("RailsParser", "Error in loading StraightRailPiece");
        }
    }

    private static void loadCurvedRailPiece(XmlPullParser pullParser){
        int prevPiece=0, prevConnection=0, nextPiece=0, nextConnection=0;
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.CurvedRPName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.PreviousName)){
                    pullParser.next();
                    String[] prev = pullParser.getText().split(",");
                    prevPiece = Integer.parseInt(prev[0]);
                    prevConnection = Integer.parseInt(prev[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.NextName)){
                    pullParser.next();
                    String[] next = pullParser.getText().split(",");
                    nextPiece = Integer.parseInt(next[0]);
                    nextConnection = Integer.parseInt(next[1]);
                }
                pullParser.next();
            }
            result.add(new CurvedRailPiece(result.get(prevPiece).getState(0, prevConnection),
                    result.get(nextPiece).getState(0, nextConnection),
                    prevPiece, prevConnection,
                    nextPiece, nextConnection,
                    vertexBufferObjectManager, texture, track));
        } catch (Exception e) {
            Log.e("RailsParser", "Error in loading CurvedRailPiece");
        }
    }

    private static void loadSwitchRailPiece(XmlPullParser pullParser){
        int prevPiece=0, prevConnection=0, next1Piece=0, next1Connection=0, next2Piece=0, next2Connection=0;
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.SwitchRPName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.PreviousName)){
                    pullParser.next();
                    String[] prev = pullParser.getText().split(",");
                    prevPiece = Integer.parseInt(prev[0]);
                    prevConnection = Integer.parseInt(prev[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.Next1Name)){
                    pullParser.next();
                    String[] next = pullParser.getText().split(",");
                    next1Piece = Integer.parseInt(next[0]);
                    next1Connection = Integer.parseInt(next[1]);
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.Next2Name)){
                    pullParser.next();
                    String[] next = pullParser.getText().split(",");
                    next2Piece = Integer.parseInt(next[0]);
                    next2Connection = Integer.parseInt(next[1]);
                }
                pullParser.next();
            }

            result.add(new SwitchRailPiece(result.get(prevPiece).getState(0, prevConnection),
                    result.get(next1Piece).getState(0, next1Connection),
                    result.get(next2Piece).getState(0, next2Connection),
                    prevPiece, prevConnection,
                    next1Piece, next1Connection,
                    next2Piece, next2Connection,
                    vertexBufferObjectManager, texture, track));
        } catch (Exception e) {
            Log.e("RailsParser", "Error in loading SwitchRailPiece");
        }
    }
}
