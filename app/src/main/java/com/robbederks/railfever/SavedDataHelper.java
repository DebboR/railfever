package com.robbederks.railfever;

import android.content.Context;
import android.content.SharedPreferences;

public class SavedDataHelper {

    private final static String PACKAGENAME = "com.robbederks.railfever";
    private final static String HIGHSCORES = "Highscores";
    private final static String COMPLETEDLEVELS = "CompletedLevels";

    private final static String NUMCOMPLETEDLEVELS = "NumCompletedLevels";

    public static void updateCompletedlevels(Context context, int numCompleted){
        int prevCompleted = getCompletedLevels(context);
        if(numCompleted > prevCompleted){
            SharedPreferences sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + COMPLETEDLEVELS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(NUMCOMPLETEDLEVELS, numCompleted);
            editor.commit();
        }
    }

    public static int getCompletedLevels(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + COMPLETEDLEVELS, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(NUMCOMPLETEDLEVELS, 0);
    }

    public static void updateHighScore(Context context, String levelFileName, int score){
        int prevScore = getHighScore(context, levelFileName);
        if(score > prevScore){
            SharedPreferences sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + HIGHSCORES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(levelFileName, score);
            editor.commit();
        }
    }

    public static int getHighScore(Context context, String levelFileName){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + HIGHSCORES, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(levelFileName, 0);
    }

    public static void resetSavedData(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + HIGHSCORES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        sharedPreferences = context.getSharedPreferences(PACKAGENAME + "." + COMPLETEDLEVELS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
