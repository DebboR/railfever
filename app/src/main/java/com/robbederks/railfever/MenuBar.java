package com.robbederks.railfever;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontUtils;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class MenuBar extends Entity implements ITouchArea {

    private final static int HEIGHT = 150;

    private VertexBufferObjectManager vertexBufferObjectManager;
    private TextureRegion background;
    private final Font font;
    private RailFeverActivity gameActivity;

    private Text timerText, payloadText;
    private ButtonSprite playPause, close;

    public MenuBar(RailFeverActivity gameActivity){
        super(0, 0);
        this.gameActivity = gameActivity;
        this.vertexBufferObjectManager = gameActivity.getVertexBufferObjectManager();
        this.background = gameActivity.menuBarBackground;
        this.font = gameActivity.fontCrystal;
        initMenu();
    }

    public void updateMenu(){
        timerText.setText(gameActivity.secondsToString(gameActivity.gameDuration));
        payloadText.setText(gameActivity.getPayloadString());
        playPause.setCurrentTileIndex(gameActivity.gamePaused ? 1 : 0);
    }

    private void initMenu(){
        timerText = new Text(110, 40, font, "00:00", vertexBufferObjectManager);
        Sprite timerIcon = new Sprite(75 - (gameActivity.watch.getWidth() / 2), 75 - (gameActivity.watch.getHeight() / 2), gameActivity.watch, vertexBufferObjectManager);
        timerIcon.setScale(font.getLineHeight()/gameActivity.watch.getWidth());

        payloadText = new Text(timerText.getX() + timerText.getWidth() + 110, 40, font, "1000/1000", vertexBufferObjectManager);
        Sprite payloadIcon = new Sprite(timerText.getX() + timerText.getWidth() + 60 - (gameActivity.yellowItem.getWidth() / 2), 75 - (gameActivity.yellowItem.getHeight() / 2), gameActivity.yellowItem, vertexBufferObjectManager);
        payloadIcon.setScale(font.getLineHeight()/gameActivity.yellowItem.getWidth());

        playPause = new ButtonSprite(RailFeverActivity.CAMERA_WIDTH - 150, 75 - (gameActivity.pause.getHeight() / 2), new TiledTextureRegion(gameActivity.atlas, gameActivity.pause, gameActivity.play), vertexBufferObjectManager){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionUp())
                    gameActivity.gamePaused = !gameActivity.gamePaused;
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        playPause.setScale(50 / gameActivity.pause.getWidth());

        close = new ButtonSprite(playPause.getX() - 80, playPause.getY(), gameActivity.menu, vertexBufferObjectManager){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionUp())
                    gameActivity.close();
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };
        close.setScale(50 / gameActivity.menu.getWidth());

        this.attachChild(new Sprite(0, 0, background, vertexBufferObjectManager));
        this.attachChild(timerIcon);
        this.attachChild(timerText);
        this.attachChild(payloadIcon);
        this.attachChild(payloadText);
        this.attachChild(playPause);
        this.attachChild(close);
    }

    @Override
    public boolean contains(float pX, float pY) {
        return pY <= HEIGHT;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(playPause.contains(pTouchAreaLocalX, pTouchAreaLocalY))
            return playPause.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
        if(close.contains(pTouchAreaLocalX, pTouchAreaLocalY))
            return close.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
        return false;
    }
}
