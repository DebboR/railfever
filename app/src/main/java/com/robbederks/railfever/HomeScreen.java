package com.robbederks.railfever;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.List;

public class HomeScreen extends Activity {

    public static Context context;
    public static final boolean DEBUG = false;

    private LinearLayout LLLevels;
    private List<Level> levels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        setContentView(R.layout.activity_home_screen);

        LLLevels = (LinearLayout) findViewById(R.id.LLLevels);
        levels = LevelHelper.getLevels();

        int completedLevels = SavedDataHelper.getCompletedLevels(context);

        for(int i=0; i<levels.size(); i++){
            Level l = levels.get(i);
            final String fileName = l.fileName;
            Button btn = new Button(this);
            btn.setText(l.levelName);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("HomeScreen", "Starting Level with fileName: " + fileName);
                    startGame(fileName);
                }
            });

            //Disable button if previous level not completed and not in DEBUG mode
            btn.setEnabled(DEBUG || i<=completedLevels);

            LLLevels.addView(btn);
        }

        Button btn = new Button(this);
        btn.setText("Clear saved data");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavedDataHelper.resetSavedData(context);
                restartActivity();
            }
        });
        LLLevels.addView(btn);
    }

    private void restartActivity(){
        startActivity(new Intent(this, HomeScreen.class));
        finish();
    }

    private void startGame(String levelFileName){
        Intent intent = new Intent(this, RailFeverActivity.class);
        intent.putExtra(LevelHelper.LevelFileNameName, levelFileName);
        startActivity(intent);
        finish();
    }
}
