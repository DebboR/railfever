package com.robbederks.railfever;

import com.robbederks.railfever.Item.Modifiers.Modifier;
import com.robbederks.railfever.Item.Modifiers.SpeedModifier;
import com.robbederks.railfever.Item.Modifiers.TurnModifier;
import com.robbederks.railfever.Rails.RailsRunner;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontUtils;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.util.ArrayList;

public class Inventory extends Entity implements ITouchArea{

    private final static int NUMMODIFIERS = 6;
    private final static int MODSIZE = 128;
    private final static int LINEWIDTH = 10;
    private final static Color BORDERCOLOR = Color.BLACK;

    private int startX, startY, width;
    private ArrayList<Modifier> modifiers;

    private TextureRegion modEFaster, modESlower, modETurn, modPFaster, modPSlower, modPTurn;
    private VertexBufferObjectManager vertexBufferObjectManager;

    private Text[] numTexts;
    private Font font;
    private RailFeverActivity gameActivity;

    public Inventory(RailFeverActivity gameActivity){
        this.gameActivity = gameActivity;
        this.modEFaster = gameActivity.modEFaster;
        this.modESlower = gameActivity.modESlower;
        this.modETurn = gameActivity.modETurn;
        this.modPFaster = gameActivity.modPFaster;
        this.modPSlower = gameActivity.modPSlower;
        this.modPTurn = gameActivity.modPTurn;
        this.vertexBufferObjectManager = gameActivity.getVertexBufferObjectManager();
        this.font = gameActivity.fontCrystal;

        this.modifiers = new ArrayList<>();
        numTexts = new Text[NUMMODIFIERS];

        width =  NUMMODIFIERS * (MODSIZE + LINEWIDTH);
        startX = (RailFeverActivity.CAMERA_WIDTH - width - LINEWIDTH) / 2;
        startY = RailFeverActivity.CAMERA_HEIGHT - MODSIZE - (LINEWIDTH / 2);

        drawInventoryNumbers();
        initInventory();
    }

    public void addModifier(Modifier mod){
        modifiers.add(mod);
        drawInventoryNumbers();
    }

    private void initInventory(){
        this.attachChild(new Line(startX - LINEWIDTH / 2, startY, startX + width + LINEWIDTH / 2, startY, LINEWIDTH, vertexBufferObjectManager));
        for(int i=0; i<=NUMMODIFIERS; i++)
            this.attachChild(new Line(startX + i*(MODSIZE + LINEWIDTH), startY, startX + i*(MODSIZE + LINEWIDTH), RailFeverActivity.CAMERA_HEIGHT, LINEWIDTH, vertexBufferObjectManager));

        for(int i=0; i<this.getChildCount(); i++)
            this.getChildByIndex(i).setColor(BORDERCOLOR);

        this.attachChild(new Sprite(startX + LINEWIDTH / 2, startY + LINEWIDTH / 2, modPSlower, vertexBufferObjectManager));
        this.attachChild(new Sprite(startX + LINEWIDTH + MODSIZE + LINEWIDTH / 2, startY + LINEWIDTH / 2, modPFaster, vertexBufferObjectManager));
        this.attachChild(new Sprite(startX + (LINEWIDTH + MODSIZE) * 2 + LINEWIDTH / 2, startY + LINEWIDTH / 2, modPTurn, vertexBufferObjectManager));
        this.attachChild(new Sprite(startX + (LINEWIDTH + MODSIZE) * 3 + LINEWIDTH / 2, startY + LINEWIDTH / 2, modESlower, vertexBufferObjectManager));
        this.attachChild(new Sprite(startX + (LINEWIDTH + MODSIZE) * 4 + LINEWIDTH / 2, startY + LINEWIDTH / 2, modEFaster, vertexBufferObjectManager));
        this.attachChild(new Sprite(startX + (LINEWIDTH + MODSIZE) * 5 + LINEWIDTH / 2, startY + LINEWIDTH / 2, modETurn, vertexBufferObjectManager));

        for(int i=0; i<NUMMODIFIERS; i++)
            this.attachChild(numTexts[i]);
    }

    private void drawInventoryNumbers(){
        int numItems[] = new int[NUMMODIFIERS];
        for(Modifier m : modifiers){
            if(m instanceof SpeedModifier){
                SpeedModifier s = (SpeedModifier) m;
                if(s.newRelativeSpeed > 1)
                    numItems[1 + (s.forPlayer ? 0 : 3)]++;
                else
                    numItems[s.forPlayer ? 0 : 3]++;
            } else if(m instanceof TurnModifier){
                numItems[2 + (m.forPlayer ? 0 : 3)]++;
            }
        }

        for (int i = 0; i < NUMMODIFIERS; i++) {
            String numStr = "" + numItems[i];
            if(numTexts[i] == null) {
                numTexts[i] = new Text(startX + LINEWIDTH + MODSIZE - FontUtils.measureText(font, numStr) - 10 + (LINEWIDTH + MODSIZE) * i, RailFeverActivity.CAMERA_HEIGHT - 60, font, "999", vertexBufferObjectManager);
                numTexts[i].setText(numStr);
            } else {
                numTexts[i].setText(numStr);
                numTexts[i].setPosition(startX + LINEWIDTH + MODSIZE - FontUtils.measureText(font, numStr) - 10 + (LINEWIDTH + MODSIZE) * i, numTexts[i].getY());
            }
        }
    }

    private void applyModifier(Class modifierClass, Boolean onPlayer){
        applyModifier(modifierClass, onPlayer, false);
    }

    private void applyModifier(Class modifierClass, Boolean onPlayer, Boolean slower){
        for(Modifier m : modifiers){
            if(m.forPlayer != onPlayer)
                continue;
            if(modifierClass.isInstance(m)){
                if(modifierClass == SpeedModifier.class)
                    if((((SpeedModifier)m).newRelativeSpeed >= 1 && slower) || (((SpeedModifier)m).newRelativeSpeed < 1 && !slower))
                        continue;
                if(onPlayer)
                    m.apply(gameActivity.playerLoc);
                else
                    m.apply(gameActivity.enemyLoc);
                modifiers.remove(m);
                break;
            }
        }
        drawInventoryNumbers();
    }

    public void resetItems(){
        modifiers.clear();
        drawInventoryNumbers();
    }

    @Override
    public boolean contains(float pX, float pY){
        return pY > startY && pX > startX && pX < startX + width;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY){
        if(pSceneTouchEvent.isActionUp()){
            int index = Math.round(pTouchAreaLocalX - startX) / (MODSIZE + LINEWIDTH);
            switch(index){
                case 0:
                    applyModifier(SpeedModifier.class, true, true);
                    break;
                case 1:
                    applyModifier(SpeedModifier.class, true, false);
                    break;
                case 2:
                    applyModifier(TurnModifier.class, true);
                    break;
                case 3:
                    applyModifier(SpeedModifier.class, false, true);
                    break;
                case 4:
                    applyModifier(SpeedModifier.class, false, false);
                    break;
                case 5:
                    applyModifier(TurnModifier.class, false);
                    break;
            }
        }
        return false;
    }
}
