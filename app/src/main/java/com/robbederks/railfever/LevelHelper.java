package com.robbederks.railfever;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class LevelHelper {

    //Constants
    public static final String FOLDER = "levels";

    public static final String LevelName = "Level";
    public static final String LevelFileNameName = "LevelFileName";
    public static final String LevelNameName = "LevelName";
    public static final String PayloadGoalName = "PayloadGoal";
    public static final String PlayerSpeedName = "PlayerSpeed";
    public static final String EnemySpeedName = "EnemySpeed";

    public static final String ItemsName = "Items";
    public static final String PlayerFastSpeedModifierName = "PlayerFastSpeedModifier";
    public static final String PlayerSlowSpeedModifierName = "PlayerSlowSpeedModifier";
    public static final String PlayerTurnModifierName = "PlayerTurnModifier";
    public static final String EnemyFastSpeedModifierName = "EnemyFastSpeedModifier";
    public static final String EnemySlowSpeedModifierName = "EnemySlowSpeedModifier";
    public static final String EnemyTurnModifierName = "EnemyTurnModifier";
    public static final String PayloadName = "Payload";
    public static final String RelativeSpeedName = "RelativeSpeed";
    public static final String DurationName = "Duration";
    public static final String SpawnRateName = "SpawnRate";
    public static final String SizeName = "Size";

    public static final String RailsName = "Rails";
    public static final String StraightRPName = "StraightRailPiece";
    public static final String CurvedRPName = "CurvedRailPiece";
    public static final String SwitchRPName = "SwitchRailPiece";
    public static final String StartPointName = "StartPoint";
    public static final String EndPointName = "EndPoint";
    public static final String PreviousName = "Previous";
    public static final String NextName = "Next";
    public static final String Next1Name = "Next1";
    public static final String Next2Name = "Next2";

    public static List<Level> getLevels(){
        ArrayList<Level> res = new ArrayList<>();
        try {
            for(String fileName : HomeScreen.context.getAssets().list(FOLDER))
                res.add(new Level(fileName));
        } catch (Exception e) {
            Log.e("LevelHelper", "Error in loading levels from directory: " + FOLDER);
        }
        return res;
    }

    public static int getLevelNr(String levelFileName){
        try {
            String[] fileNames = HomeScreen.context.getAssets().list(FOLDER);
            for(int i=0; i<fileNames.length; i++){
                if(fileNames[i].equals(levelFileName))
                    return i + 1;
            }
        } catch (Exception e) {
            Log.e("LevelHelper", "Error in finding LevelNr from directory: " + FOLDER);
        }
        return -1;
    }
}
