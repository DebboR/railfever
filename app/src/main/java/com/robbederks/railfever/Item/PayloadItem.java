package com.robbederks.railfever.Item;

import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.Rails;
import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class PayloadItem extends Item {

    public static int size;
    public static int spawnRate;

    public PayloadItem(RailFeverActivity gameActivity, int Railpiece, int RailIndex){
        super(gameActivity, Railpiece, RailIndex, gameActivity.yellowItem);
    }
    public void apply(Locomotive loc){
        loc.carriedPayload += size;
    }
}
