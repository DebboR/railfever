package com.robbederks.railfever.Item.Modifiers;

import com.robbederks.railfever.Item.Item;
import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.RailState;
import com.robbederks.railfever.Rails.Rails;
import com.robbederks.railfever.Rails.RailsRunner;
import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class Modifier extends Item{

    public boolean forPlayer;

    protected Modifier(RailFeverActivity gameActivity, int railPiece, int railIndex, boolean forPlayer, TextureRegion textureRegion) {
        super(gameActivity, railPiece, railIndex, textureRegion);
        this.forPlayer = forPlayer;
    }
}
