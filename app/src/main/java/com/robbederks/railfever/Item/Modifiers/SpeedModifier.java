package com.robbederks.railfever.Item.Modifiers;

import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.Rails;
import com.robbederks.railfever.Rails.RailsRunner;
import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class SpeedModifier extends Modifier {

    public double newRelativeSpeed;

    public static int pFastDuration, pSlowDuration, eFastDuration, eSlowDuration;
    public static double pFastRelativeSpeed, pSlowRelativeSpeed, eFastRelativeSpeed, eSlowRelativeSpeed;
    public static int pFastSpawnRate, pSlowSpawnRate, eFastSpawnRate, eSlowSpawnRate;

    public SpeedModifier(RailFeverActivity gameActivity, int railPiece, int railIndex, boolean slow, boolean forPlayer){
        super(gameActivity, railPiece, railIndex, forPlayer, slow ? (forPlayer ? gameActivity.modPSlower : gameActivity.modESlower) : (forPlayer ? gameActivity.modPFaster : gameActivity.modEFaster));
        this.newRelativeSpeed = slow ? (forPlayer ? pSlowRelativeSpeed : eSlowRelativeSpeed) : (forPlayer ? pFastRelativeSpeed : eFastRelativeSpeed);
    }

    @Override
    public void apply(Locomotive loc){
        int duration;
        if(forPlayer)
            duration = newRelativeSpeed >= 1 ? pFastDuration : pSlowDuration;
        else
            duration = newRelativeSpeed >= 1 ? eFastDuration : eSlowDuration;
        loc.runner.setSpeed(loc.runner.baseSpeed * newRelativeSpeed, duration);
    }
}
