package com.robbederks.railfever.Item.Modifiers;


import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.Rails;

import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class TurnModifier extends Modifier {

    public static int pSpawnRate, eSpawnRate;

    public TurnModifier(RailFeverActivity gameActivity, int railPiece, int railIndex, boolean forPlayer){
        super(gameActivity, railPiece, railIndex, forPlayer, forPlayer ? gameActivity.modPTurn : gameActivity.modETurn);
    }

    @Override
    public void apply(Locomotive loc){
        loc.runner.turn();
    }
}
