package com.robbederks.railfever.Item;

import com.robbederks.railfever.Item.Modifiers.SpeedModifier;
import com.robbederks.railfever.Item.Modifiers.TurnModifier;
import com.robbederks.railfever.RailFeverActivity;

import java.util.Random;

public class ItemSpawner {

    private RailFeverActivity gameActivity;
    private static Random random = new Random();

    private int pFastSpeedMod, eFastSpeedMod, pSlowSpeedMod, eSlowSpeedMod, pTurnMod, eTurnMod, payload;
    private int randomRailPiece, randomRailIndex;

    public ItemSpawner(RailFeverActivity gameActivity){
        this.gameActivity = gameActivity;
        generateRandomPosition();
        generateRandomStartValues();
    }

    private void generateRandomStartValues(){
        pFastSpeedMod = random.nextInt(SpeedModifier.pFastSpawnRate * RailFeverActivity.FPS / 1000);
        eFastSpeedMod = random.nextInt(SpeedModifier.eFastSpawnRate * RailFeverActivity.FPS / 1000);
        pSlowSpeedMod = random.nextInt(SpeedModifier.pSlowSpawnRate * RailFeverActivity.FPS / 1000);
        eSlowSpeedMod = random.nextInt(SpeedModifier.eSlowSpawnRate * RailFeverActivity.FPS / 1000);
        pTurnMod = random.nextInt(TurnModifier.pSpawnRate * RailFeverActivity.FPS / 1000);
        eTurnMod = random.nextInt(TurnModifier.eSpawnRate * RailFeverActivity.FPS / 1000);
        payload = random.nextInt(PayloadItem.spawnRate * RailFeverActivity.FPS / 1000);
    }

    private void generateRandomPosition(){
        randomRailPiece = random.nextInt(gameActivity.level.rails.pieces.size());
        randomRailIndex = random.nextInt(gameActivity.level.rails.pieces.get(randomRailPiece).getLength());
    }

    public void resetPayload(){
        payload = PayloadItem.spawnRate * RailFeverActivity.FPS / 1000;
    }

    public void step(){
        pFastSpeedMod--; eFastSpeedMod--; pSlowSpeedMod--; eSlowSpeedMod--; pTurnMod--; eTurnMod--; payload--;
        if(pFastSpeedMod <= 0){
            pFastSpeedMod = SpeedModifier.pFastSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new SpeedModifier(gameActivity, randomRailPiece, randomRailIndex, false, true));
            generateRandomPosition();
        }
        if(eFastSpeedMod <= 0){
            eFastSpeedMod = SpeedModifier.eFastSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new SpeedModifier(gameActivity, randomRailPiece, randomRailIndex, false, false));
            generateRandomPosition();
        }
        if(pSlowSpeedMod <= 0){
            pSlowSpeedMod = SpeedModifier.pSlowSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new SpeedModifier(gameActivity, randomRailPiece, randomRailIndex, true, true));
            generateRandomPosition();
        }
        if(eSlowSpeedMod <= 0){
            eSlowSpeedMod = SpeedModifier.eSlowSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new SpeedModifier(gameActivity, randomRailPiece, randomRailIndex, true, false));
            generateRandomPosition();
        }
        if(pTurnMod <= 0){
            pTurnMod = TurnModifier.pSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new TurnModifier(gameActivity, randomRailPiece, randomRailIndex, true));
            generateRandomPosition();
        }
        if(eTurnMod <= 0){
            eTurnMod = TurnModifier.eSpawnRate * RailFeverActivity.FPS / 1000;
            gameActivity.addItem(new TurnModifier(gameActivity, randomRailPiece, randomRailIndex, false));
            generateRandomPosition();
        }
        if(payload == 0){
            payload = -1;
            gameActivity.addItem(new PayloadItem(gameActivity, randomRailPiece, randomRailIndex));
            generateRandomPosition();
        }
    }
}
