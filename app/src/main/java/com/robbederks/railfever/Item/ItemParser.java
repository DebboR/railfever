package com.robbederks.railfever.Item;

import android.util.Log;

import com.robbederks.railfever.Item.Modifiers.SpeedModifier;
import com.robbederks.railfever.Item.Modifiers.TurnModifier;
import com.robbederks.railfever.LevelHelper;
import com.robbederks.railfever.RailFeverActivity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class ItemParser {

    public static void loadXML(String fileName){
        XmlPullParserFactory pullParserFactory;
        XmlPullParser pullParser;
        try{
            pullParserFactory = XmlPullParserFactory.newInstance();
            pullParser = pullParserFactory.newPullParser();
            pullParser.setInput(RailFeverActivity.context.getAssets().open(LevelHelper.FOLDER + "/" + fileName), null);

            while(!(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.ItemsName)) && !(pullParser.getEventType() == XmlPullParser.END_DOCUMENT)){
                pullParser.next();
            }

            while(pullParser.getEventType() != XmlPullParser.END_DOCUMENT && !(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.ItemsName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG){
                    switch(pullParser.getName()){
                        case LevelHelper.PayloadName:
                            loadPayload(pullParser);
                            break;
                        case LevelHelper.PlayerFastSpeedModifierName:
                            loadPFastSpeedModifier(pullParser);
                            break;
                        case LevelHelper.EnemyFastSpeedModifierName:
                            loadEFastSpeedModifier(pullParser);
                            break;
                        case LevelHelper.PlayerSlowSpeedModifierName:
                            loadPSlowSpeedModifier(pullParser);
                            break;
                        case LevelHelper.EnemySlowSpeedModifierName:
                            loadESlowSpeedModifier(pullParser);
                            break;
                        case LevelHelper.PlayerTurnModifierName:
                            loadPTurnModifier(pullParser);
                            break;
                        case LevelHelper.EnemyTurnModifierName:
                            loadETurnModifier(pullParser);
                            break;
                        default:
                            break;
                    }
                }
                pullParser.next();
            }
        } catch(Exception e) {
            Log.e("ItemParser", "Error in loading level from file: " + fileName);
        }
    }

    private static void loadPayload(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.PayloadName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SizeName)){
                    pullParser.next();
                    PayloadItem.size = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    PayloadItem.spawnRate = Integer.parseInt(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading Payload");
        }
    }

    private static void loadPFastSpeedModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.PlayerFastSpeedModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.DurationName)){
                    pullParser.next();
                    SpeedModifier.pFastDuration = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    SpeedModifier.pFastSpawnRate = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.RelativeSpeedName)){
                    pullParser.next();
                    SpeedModifier.pFastRelativeSpeed = Double.parseDouble(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading PFastSpeedModifier");
        }
    }
    private static void loadEFastSpeedModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.EnemyFastSpeedModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.DurationName)){
                    pullParser.next();
                    SpeedModifier.eFastDuration = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    SpeedModifier.eFastSpawnRate = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.RelativeSpeedName)){
                    pullParser.next();
                    SpeedModifier.eFastRelativeSpeed = Double.parseDouble(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading EFastSpeedModifier");
        }
    }

    private static void loadPSlowSpeedModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.PlayerSlowSpeedModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.DurationName)){
                    pullParser.next();
                    SpeedModifier.pSlowDuration = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    SpeedModifier.pSlowSpawnRate = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.RelativeSpeedName)){
                    pullParser.next();
                    SpeedModifier.pSlowRelativeSpeed = Double.parseDouble(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading PSlowSpeedModifier");
        }
    }

    private static void loadESlowSpeedModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.EnemySlowSpeedModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.DurationName)){
                    pullParser.next();
                    SpeedModifier.eSlowDuration = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    SpeedModifier.eSlowSpawnRate = Integer.parseInt(pullParser.getText());
                } else if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.RelativeSpeedName)){
                    pullParser.next();
                    SpeedModifier.eSlowRelativeSpeed = Double.parseDouble(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading ESlowSpeedModifier");
        }
    }

    private static void loadPTurnModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.PlayerTurnModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    TurnModifier.pSpawnRate = Integer.parseInt(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading PTurnModifier");
        }
    }

    private static void loadETurnModifier(XmlPullParser pullParser){
        try {
            while(!(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.EnemyTurnModifierName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.SpawnRateName)){
                    pullParser.next();
                    TurnModifier.eSpawnRate = Integer.parseInt(pullParser.getText());
                }
                pullParser.next();
            }
        } catch (Exception e) {
            Log.e("ItemParser", "Error in loading ETurnModifier");
        }
    }
}
