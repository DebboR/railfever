package com.robbederks.railfever.Item;

import org.andengine.entity.sprite.Sprite;

import com.robbederks.railfever.RailFeverActivity;
import com.robbederks.railfever.Rails.RailState;
import com.robbederks.railfever.Rails.Rails;
import com.robbederks.railfever.Rails.RailsRunner;
import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class Item extends Sprite {

    private Rails rails;
    private final int railPiece, railIndex;
    protected final TextureRegion textureRegion;
    protected final int XOFFSET, YOFFSET;
    protected final static int ITEMSIZEONRAIL = 40;


    protected Item(RailFeverActivity gameActivity, int railPiece, int railIndex, TextureRegion textureRegion){
        super(0, 0, textureRegion, gameActivity.getVertexBufferObjectManager());

        XOFFSET = Math.round(textureRegion.getWidth() /2);
        YOFFSET = Math.round(textureRegion.getHeight() /2);

        this.rails = gameActivity.level.rails;
        this.railPiece = railPiece;
        this.railIndex = railIndex;
        this.textureRegion = textureRegion;
        RailState state = rails.pieces.get(this.railPiece).getState(this.railIndex, 0);
        this.setPosition(state.x - XOFFSET, state.y - YOFFSET);
        this.setScale(((float) ITEMSIZEONRAIL) / textureRegion.getWidth(), ((float) ITEMSIZEONRAIL) / textureRegion.getHeight());
    }

    public abstract void apply(Locomotive loc);
}
