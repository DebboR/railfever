package com.robbederks.railfever.Trains;

import com.robbederks.railfever.Rails.RailState;
import com.robbederks.railfever.Rails.RailsRunner;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Locomotive extends Sprite{

    private final int XOFFSET;
    private final int YOFFSET;

    public int carriedPayload = 0;

    public RailsRunner runner;

    public Locomotive(RailsRunner runner, ITextureRegion textureRegion, VertexBufferObjectManager vertexBufferObjectManager){
        super(runner.current.x, runner.current.y, textureRegion, vertexBufferObjectManager);
        XOFFSET = Math.round(textureRegion.getWidth()/2);
        YOFFSET = Math.round(textureRegion.getHeight()/2);
        this.runner = runner;
        this.setPosition(runner.current.x - XOFFSET, runner.current.y - YOFFSET);
        this.setRotation((float) (runner.current.rot * 180 / RailState.PI));
    }

    public void next(){
        setRailState(runner.next());
    }

    public void resetLocomotive(){
        carriedPayload = 0;
        runner.resetRailsRunner();
    }

    public void setRailState(RailState state) {
        this.setPosition(state.x - XOFFSET, state.y - YOFFSET);
        this.setRotation((float) (state.rot * 180 / RailState.PI));
    }
}
