package com.robbederks.railfever;

import android.util.Log;

import com.robbederks.railfever.Item.ItemParser;
import com.robbederks.railfever.Rails.Rails;

import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class Level {

    public String levelName;
    public String fileName;
    public int payloadGoal;
    public Rails rails;
    public double playerSpeed, enemySpeed;

    public Level(String fileName){
        this(fileName, false);
    }

    public Level(String fileName, boolean loadItems){
        this.fileName = fileName;
        parseLevel();
        if(loadItems)
            ItemParser.loadXML(fileName);
    }

    public Level(String fileName, VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        this(fileName, true);
        loadRails(vertexBufferObjectManager, texture, track);
    }

    private void parseLevel(){
        XmlPullParserFactory pullParserFactory;
        XmlPullParser pullParser;

        try{
            pullParserFactory = XmlPullParserFactory.newInstance();
            pullParser = pullParserFactory.newPullParser();
            pullParser.setInput(HomeScreen.context.getAssets().open(LevelHelper.FOLDER + "/" + fileName), null);

            while(!(pullParser.getEventType() == XmlPullParser.START_TAG && pullParser.getName().equals(LevelHelper.LevelName))){
                pullParser.next();
            }

            while(pullParser.getEventType() != XmlPullParser.END_DOCUMENT && !(pullParser.getEventType() == XmlPullParser.END_TAG && pullParser.getName().equals(LevelHelper.LevelName))){
                if(pullParser.getEventType() == XmlPullParser.START_TAG){
                    switch(pullParser.getName()){
                        case LevelHelper.LevelNameName:
                            pullParser.next();
                            levelName = pullParser.getText();
                            break;
                        case LevelHelper.PayloadGoalName:
                            pullParser.next();
                            payloadGoal = Integer.parseInt(pullParser.getText());
                            break;
                        case LevelHelper.PlayerSpeedName:
                            pullParser.next();
                            playerSpeed = Double.parseDouble(pullParser.getText());
                            break;
                        case LevelHelper.EnemySpeedName:
                            pullParser.next();
                            enemySpeed = Double.parseDouble(pullParser.getText());
                            break;
                        default:
                            break;
                    }
                }
                pullParser.next();
            }


        } catch (Exception e) {
            Log.e("LevelParser", "Error in loading level from file: " + fileName);
        }
    }

    public void loadRails(VertexBufferObjectManager vertexBufferObjectManager, Texture texture, TextureRegion track){
        rails = new Rails(fileName, vertexBufferObjectManager, texture, track);
    }
}
