package com.robbederks.railfever;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
public class GameOverScene extends Scene {

    private final RailFeverActivity gameActivity;

    private final int startX, startY, width, height;

    private VertexBufferObjectManager vertexBufferObjectManager;
    private final Font font;

    private Text timerText, payloadText;
    private TiledSprite gameOverHeader;

    public GameOverScene(final RailFeverActivity gameActivity){
        super();
        this.gameActivity = gameActivity;
        this.vertexBufferObjectManager = gameActivity.getVertexBufferObjectManager();
        this.font = gameActivity.fontCrystalBig;

        //X and Y offset of the menu
        width = (int) gameActivity.gameOverBackground.getWidth();
        height = (int) gameActivity.gameOverBackground.getHeight();
        startX = (RailFeverActivity.CAMERA_WIDTH - width)/2;
        startY = (RailFeverActivity.CAMERA_HEIGHT - height)/2;

        //Add background
        setBackgroundEnabled(false);
        attachChild(new Sprite(startX, startY, gameActivity.gameOverBackground, vertexBufferObjectManager));

        //Game over header
        gameOverHeader = new TiledSprite(startX + ((width - (int) gameActivity.gameOver.getWidth()) / 2), startY + 100, new TiledTextureRegion(gameActivity.bigPictureAtlas, gameActivity.gameOver, gameActivity.gameWon), vertexBufferObjectManager);
        attachChild(gameOverHeader);

        //Restart and menu button
        ButtonSprite close = new ButtonSprite(startX + (width/2) - (gameActivity.menu.getWidth() + 100), startY + height - (gameActivity.menu.getHeight() + 150), gameActivity.menu, vertexBufferObjectManager){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionUp())
                    gameActivity.close();
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };

        ButtonSprite reset = new ButtonSprite(startX + (width/2) + 100, startY + height - (gameActivity.reset.getHeight() + 150), gameActivity.reset, vertexBufferObjectManager){
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if(pSceneTouchEvent.isActionUp())
                    gameActivity.resetGame();
                return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            }
        };

        timerText = new Text(startX + ((width - (int)gameActivity.gameOver.getWidth())/2), startY + 300, font, "00:00", vertexBufferObjectManager);
        Sprite timerIcon = new Sprite((startX +(width - (int)gameActivity.watch.getWidth()) / 2)-200, startY + 310, gameActivity.watch, vertexBufferObjectManager);
        timerIcon.setScale(font.getLineHeight()/gameActivity.watch.getWidth());

        payloadText = new Text(startX + ((width - (int)gameActivity.gameOver.getWidth())/2), startY + 450, font, "1000/1000", vertexBufferObjectManager);
        Sprite payloadIcon = new Sprite((startX + (width -(int)gameActivity.yellowItem.getWidth() )/ 2)-200, startY + 450, gameActivity.yellowItem, vertexBufferObjectManager);
        payloadIcon.setScale(font.getLineHeight() / gameActivity.yellowItem.getWidth());

        attachChild(timerIcon);
        attachChild(timerText);
        attachChild(payloadIcon);
        attachChild(payloadText);
        registerTouchArea(close);
        registerTouchArea(reset);
        attachChild(close);
        attachChild(reset);
    }

    public void updateGameOverScene(boolean gameWon){
        timerText.setText(gameActivity.secondsToString(gameActivity.gameDuration));
        payloadText.setText(gameActivity.getPayloadString());
        gameOverHeader.setCurrentTileIndex(gameWon ? 1 : 0);
    }
}
