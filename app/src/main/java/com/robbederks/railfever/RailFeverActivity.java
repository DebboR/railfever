package com.robbederks.railfever;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import com.robbederks.railfever.Item.Item;
import com.robbederks.railfever.Item.ItemSpawner;
import com.robbederks.railfever.Item.Modifiers.Modifier;
import com.robbederks.railfever.Item.Modifiers.SpeedModifier;
import com.robbederks.railfever.Item.Modifiers.TurnModifier;
import com.robbederks.railfever.Item.PayloadItem;
import com.robbederks.railfever.Rails.RailPiece.RailPiece;
import com.robbederks.railfever.Rails.RailsRunner;
import com.robbederks.railfever.Trains.Locomotive;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.opengl.font.Font;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class RailFeverActivity extends SimpleBaseGameActivity {

    public static Context context;

    private Camera camera;
    private Scene mainScene;
    public static final int CAMERA_WIDTH = 1080;
    public static final int CAMERA_HEIGHT = 1920;
    public static final int FPS = 100;

    public BitmapTextureAtlas atlas, bigPictureAtlas;
    public Font fontCrystal, fontCrystalBig;
    public TextureRegion train1, track, train2, yellowItem, watch;
    public TextureRegion modEFaster, modESlower, modETurn, modPFaster, modPSlower, modPTurn;
    public TextureRegion background, menuBarBackground, gameOverBackground, gameOver, gameWon;
    public TextureRegion menu, pause, play, reset;

    public Locomotive playerLoc, enemyLoc;
    public int gameDuration = 0;
    private ArrayList<Item> items;
    private int modifiersOnScreen = 0;
    public boolean gamePaused = false;
    private ItemSpawner itemSpawner;

    public Level level;
    private Inventory inventory;
    private MenuBar menuBar;
    private GameOverScene gameOverScene;

    private Timer gameTimer, frameTimer;

    @Override
    protected void onCreateResources() {
        //Bitmaps
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("");
        atlas = new BitmapTextureAtlas(getTextureManager(), 2048, 512, TextureOptions.DEFAULT);
        bigPictureAtlas = new BitmapTextureAtlas(getTextureManager(), 4096, 2048, TextureOptions.DEFAULT);
        int xPos = 0;

        //Big pictures
        background = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bigPictureAtlas, this, "gfx/background.png", xPos, 0);
        xPos += 1080;
        menuBarBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bigPictureAtlas, this, "gfx/menuBarBackground.png", xPos, 0);
        xPos += 1080;
        gameOverBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bigPictureAtlas, this, "gfx/gameOverBackground.png", xPos, 0);
        xPos += 750;
        gameOver = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bigPictureAtlas, this, "gfx/gameOver.png", xPos, 0);
        xPos += 256;
        gameWon = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bigPictureAtlas, this, "gfx/gameWon.png", xPos, 0);

        //Smaller pictures
        xPos = 0;
        train1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/train1.png", xPos, 0);
        xPos += 95;
        track = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/track.png", xPos, 0);
        xPos += 8;
        train2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this,"gfx/train2.png", xPos, 0);
        xPos += 95;
        yellowItem = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/yellowItem.png", xPos, 0);
        xPos += 100;
        modEFaster = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modEFaster.png", xPos, 0);
        xPos += 128;
        modESlower = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modESlower.png", xPos, 0);
        xPos += 128;
        modETurn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modETurn.png", xPos, 0);
        xPos += 128;
        modPFaster = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modPFaster.png", xPos, 0);
        xPos += 128;
        modPSlower = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modPSlower.png", xPos, 0);
        xPos += 128;
        modPTurn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/mod/modPTurn.png", xPos, 0);
        xPos += 128;
        menu = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/menu.png", xPos, 0);
        xPos += 128;
        pause = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/pause.png", xPos, 0);
        xPos += 128;
        play = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/play.png", xPos, 0);
        xPos += 128;
        reset = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/reset.png", xPos, 0);
        xPos += 128;
        watch = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, this, "gfx/watch.png", xPos, 0);

        bigPictureAtlas.load();
        atlas.load();

        //Fonts
        fontCrystal = FontFactory.createFromAsset(this.getFontManager(), this.getTextureManager(), 256, 256, this.getAssets(), "fonts/Crystal.ttf", 50, true, Color.BLACK);
        fontCrystal.load();
        fontCrystalBig = FontFactory.createFromAsset(this.getFontManager(), this.getTextureManager(), 256, 256, this.getAssets(), "fonts/Crystal.ttf", 100, true, Color.BLACK);
        fontCrystalBig.load();
    }

    @Override
    protected Scene onCreateScene() {
        context = getApplicationContext();
        mainScene = new Scene();
        mainScene.setBackground(new SpriteBackground(new Sprite(0, 0, background, getVertexBufferObjectManager())));

        level = new Level(getIntent().getStringExtra(LevelHelper.LevelFileNameName), getVertexBufferObjectManager(), atlas, track);
        items = new ArrayList<>();
        itemSpawner = new ItemSpawner(this);

        inventory = new Inventory(this);
        menuBar = new MenuBar(this);
        gameOverScene = new GameOverScene(this);

        //TODO: Change spawnlocation of locomotives

        playerLoc = new Locomotive(new RailsRunner(level.rails, 0, 0, 0, (int)(train1.getWidth() / RailPiece.RESOLUTION) / 2, (float) level.playerSpeed), train1, getVertexBufferObjectManager());
        enemyLoc = new Locomotive(new RailsRunner(level.rails, 1, 0, 0, (int)(train1.getWidth() / RailPiece.RESOLUTION) / 2, (float) level.enemySpeed), train2, getVertexBufferObjectManager());

        mainScene.attachChild(level.rails);
        mainScene.registerTouchArea(level.rails);
        mainScene.attachChild(playerLoc);
        mainScene.attachChild(enemyLoc);
        mainScene.attachChild(inventory);
        mainScene.registerTouchArea(inventory);
        mainScene.attachChild(menuBar);
        mainScene.registerTouchArea(menuBar);

        resetGame();

        return mainScene;
    }

    private void scheduleTimers(){
        frameTimer = new Timer();
        gameTimer = new Timer();

        frameTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!gamePaused){
                    playerLoc.next();
                    enemyLoc.next();
                    checkCollisions(playerLoc);
                    if(playerLoc.carriedPayload >= level.payloadGoal){
                        gameWon();
                    }
                }
                menuBar.updateMenu();
                itemSpawner.step();
            }
        }, 0, 1000/FPS);

        gameTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!gamePaused)
                    gameDuration++;
            }
        }, 0, 1000);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        return new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, new FillResolutionPolicy(), camera);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        frameTimer.cancel();
        gameTimer.cancel();
    }

    public void addItem(Item i){
        if(i instanceof Modifier)
            modifiersOnScreen++;
        items.add(i);
        mainScene.attachChild(i);
        Log.i(this.getClass().getSimpleName(), "Added item!");
    }

    private int getScore(){
        return Math.max(1000 - gameDuration, 0);
    }

    public void resetGame(){
        for(Entity e : items)
            removeEntity(e);
        items.clear();
        gameDuration = 0;
        inventory.resetItems();
        playerLoc.resetLocomotive();
        enemyLoc.resetLocomotive();
        mainScene.clearChildScene();
        scheduleTimers();
    }

    public void close(){
        frameTimer.cancel();
        gameTimer.cancel();
        startActivity(new Intent(this, HomeScreen.class));
        finish();
    }

    private void gameOver(){
        Log.i(getClass().getSimpleName(), "Game Over!");
        frameTimer.cancel();
        gameTimer.cancel();
        gameOverScene.updateGameOverScene(false);
        mainScene.setChildScene(gameOverScene);
    }

    private void gameWon(){
        Log.i(getClass().getSimpleName(), "Game Won!");
        frameTimer.cancel();
        gameTimer.cancel();
        SavedDataHelper.updateHighScore(this.getApplicationContext(), level.fileName, getScore());
        SavedDataHelper.updateCompletedlevels(this.getApplicationContext(), LevelHelper.getLevelNr(level.fileName));
        gameOverScene.updateGameOverScene(true);
        mainScene.setChildScene(gameOverScene);
    }

    public String getPayloadString(){
        int numDigits = String.valueOf(this.level.payloadGoal).length();
        return String.format("%0" + numDigits + "d", this.playerLoc.carriedPayload) + "/" + this.level.payloadGoal;
    }

    public String secondsToString(int sec){
        return String.format("%02d", sec/60) + ":" + String.format("%02d", sec%60);
    }

    private void removeEntity(final Entity e){
        if(e instanceof Modifier)
            modifiersOnScreen--;
        this.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                mainScene.detachChild(e);
            }
        });
    }

    private void checkCollisions(Locomotive loc){
        //Check for items on rails
        Iterator<Item> it = items.iterator();
        while(it.hasNext()){
            Item i = it.next();
            if(loc.collidesWith(i)){
                if(i instanceof Modifier)
                    inventory.addModifier((Modifier) i);
                else if(i instanceof PayloadItem){
                    i.apply(loc);
                    itemSpawner.resetPayload();
                } else
                    i.apply(loc);
                removeEntity(i);
                it.remove();
            }
        }

        //Check for collision with enemy
        if(loc.collidesWith(enemyLoc))
            gameOver();
    }
}
